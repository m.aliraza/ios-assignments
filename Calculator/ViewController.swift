//
//  ViewController.swift
//  Calculator
//
//  Created by Muhammad Ali on 16/07/2019.
//  Copyright © 2019 Muhammad Ali. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var number:Double=0;
    var prevNumber:Double=0;
    var calculation=false;
    var result=0;
    
    @IBOutlet weak var lbl: UILabel!
    @IBAction func numbers(_ sender: UIButton) {
        if calculation == true
        {
            lbl.text = String(sender.tag-1)
            number = Double(lbl.text!)!
            calculation=false
        }
        else{
            lbl.text=lbl.text! + String(sender.tag-1)
            number=Double(lbl.text!)!
        }
       
    }
    @IBAction func operation_button(_ sender: UIButton) {
        if lbl.text != "" && sender.tag != 11 && sender.tag != 16{
            
            prevNumber=Double(lbl.text!)!
            if sender.tag == 12{
                lbl.text="/";
            }
            else if sender.tag == 13{
                lbl.text="-";
            }
            else if sender.tag == 14{
                lbl.text="x";
            }
            else if sender.tag == 15{
                lbl.text="+";
            }
            result=sender.tag
            calculation=true;
            
            }
        else if sender.tag == 16{
            if result == 12 {
                lbl.text=String(prevNumber / number)
            }
            else if result == 13{
                lbl.text=String(prevNumber - number)
            }
            else if result == 14{
                lbl.text=String(prevNumber * number)
            }
            else if result == 15{
                lbl.text=String(prevNumber + number)
            }
           
        }
        else if sender.tag == 11{
                lbl.text=""
                number=0
                prevNumber=0
                result=0
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}
